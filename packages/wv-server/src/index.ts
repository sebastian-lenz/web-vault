/// <reference path="./index.d.ts" />

import 'reflect-metadata';
import * as yargs from 'yargs';
import { resolve } from 'path';

import './services';
import runJobs from './utils/runJobs';
import runServer from './utils/runServer';

yargs
  .option('dataPath', {
    describe: 'Path to the data directory',
    default:
      process.env.WV_DATAPATH || resolve(__dirname, '..', '..', '..', 'data'),
  })
  .option('securePath', {
    describe: 'Path to the secure directory',
    default:
      process.env.WV_SECUREPATH ||
      resolve(__dirname, '..', '..', '..', 'secure'),
  })
  .command('run', 'Run the cronjob', () => {}, runJobs)
  .command(
    ['start [port]', '*'],
    'Start the GraphQL server',
    yargs =>
      yargs.option('port', {
        describe: 'Port to bind on',
        default: 4000,
      }),
    runServer
  ).argv;
