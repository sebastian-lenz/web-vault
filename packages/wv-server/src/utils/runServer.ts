import * as express from 'express';
import { ApolloServer } from 'apollo-server-express';
import { buildSchema } from 'type-graphql';
import { Container } from 'typedi';

import defaultDocument from './defaultDocument';
import readOptions, { Options } from './readOptions';
import { resolvers } from '../models';
import Scheduler from '../services/Scheduler';

export interface StartOptions extends Options {
  port: number;
}

export default async function runServer({ port, ...options }: StartOptions) {
  await readOptions(options);
  Container.get(Scheduler);

  const schema = await buildSchema({
    container: Container,
    resolvers: [resolvers],
  });

  const app = express();
  const server = new ApolloServer({ schema });
  server.applyMiddleware({ app });
  app
    .use(express.static('../wv-client/dist'))
    .use(defaultDocument)
    .listen({ port }, () => {
      console.log(
        `Server is running, GraphQL Playground available at ${server.graphqlPath}`
      );
    });
}
