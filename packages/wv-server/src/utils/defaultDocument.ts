import { NextFunction, Request, Response, Express } from 'express';

export default function defaultDocument(
  req: Request,
  res: Response,
  next: NextFunction
) {
  res.send(`<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8" />
      <title>WebVault</title>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
    </head>
  
    <body>
      <div id="wv-client"></div>
      <script src="/client.js"></script>
      <script src="http://localhost:35729/livereload.js"></script>
    </body>
  </html>
  `);
}
