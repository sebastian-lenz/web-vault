import * as dns from 'dns-socket';

export interface Settings {
  timeout: number;
  retries: number;
  domainName: string;
  port: number;
  host: string;
}

export default function isOnline({
  domainName = 'google.com',
  host = '8.8.8.8',
  port = 53,
  retries = 5,
  timeout = 60000,
}: Partial<Settings> = {}) {
  return new Promise((resolve, reject) => {
    const socket = dns({
      timeout: timeout,
      retries: retries,
    });

    socket.query(
      {
        questions: [
          {
            type: 'A',
            name: domainName,
          },
        ],
      },
      port,
      host
    );

    socket.on('response', () => {
      socket.destroy(() => {
        resolve();
      });
    });

    socket.on('timeout', () => {
      socket.destroy(() => {
        reject();
      });
    });
  });
}
