import { Container } from 'typedi';

import readOptions, { Options } from './readOptions';
import Backup from '../services/Backup';

export default async function runJobs(options: Options) {
  await readOptions(options);
  const backup = Container.get(Backup);

  return backup.executeAll();
}
