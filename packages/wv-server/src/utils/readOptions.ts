import Container from 'typedi';

import SSHKeyCollection from '../models/ssh-key/SSHKeyCollection';
import GlobalSettings from '../services/GlobalSettings';

export interface Options {
  dataPath: string;
  securePath: string;
}

export default async function readOptions({ dataPath, securePath }: Options) {
  const settings = Container.get(GlobalSettings);
  settings.apply({ dataPath, securePath });

  const sshKeys = Container.get(SSHKeyCollection);
  await sshKeys.ensureSSHKeys();
}
