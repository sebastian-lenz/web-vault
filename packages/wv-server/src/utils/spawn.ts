import { spawn as spawnNative, SpawnOptionsWithoutStdio } from 'child_process';

export default function spawn(
  command: string,
  args: Array<string>,
  options?: SpawnOptionsWithoutStdio
): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    const process = spawnNative(command, args, options);

    const stdOut: Array<string> = [];
    const stdError: Array<string> = [];
    process.stdout.on('data', chunk => stdOut.push(`${chunk}`));
    process.stderr.on('data', chunk => stdError.push(`${chunk}`));

    process
      .on('exit', (code: number) => {
        if (!code) {
          resolve(stdOut.join(''));
        } else {
          reject(new Error(stdError.join('')));
        }
      })
      .on('error', error => {
        reject(error);
      });
  });
}
