import { Field, InputType } from 'type-graphql';

@InputType()
export default class ProjectPayload {
  @Field() description!: string;
  @Field() name!: string;
  @Field() sshHost!: string;
  @Field() sshKey!: string;
  @Field() sshPort!: number;
  @Field() sshTaskFile!: string;
  @Field() sshUser!: string;
  @Field() title!: string;
}
