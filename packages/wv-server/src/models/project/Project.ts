import { Field, ObjectType } from 'type-graphql';
import { join } from 'path';
import { readFile, writeFile } from 'fs-extra';

export interface SerializedProject {
  created: string;
  description: string;
  sshHost: string;
  sshKey: string;
  sshPort: number;
  sshTaskFile: string;
  sshUser: string;
  title: string;
}

@ObjectType()
export default class Project {
  path: string;

  @Field() created: Date = new Date();
  @Field() description: string = '';
  @Field() name: string;
  @Field() sshHost: string = '';
  @Field() sshKey: string = '';
  @Field() sshPort: number = 22;
  @Field() sshTaskFile: string = '';
  @Field() sshUser: string = '';
  @Field() title: string;

  constructor(name: string, path: string) {
    this.name = name;
    this.path = path;
    this.title = name;
  }

  apply(data: any) {
    for (const key in data) {
      const value = data[key] as any;
      switch (key) {
        case 'created':
          this.created = new Date(value);
          break;

        case 'sshPort':
          this[key] = value * 1;
          break;

        case 'description':
        case 'sshHost':
        case 'sshKey':
        case 'sshTaskFile':
        case 'sshUser':
        case 'title':
          this[key] = value;
      }
    }
  }

  getConfigFile(): string {
    return join(this.path, '.wvproject');
  }

  getTaskFile(): string {
    return this.sshTaskFile ? this.sshTaskFile : '~/.wvtasks';
  }

  async load() {
    const config = await readFile(this.getConfigFile(), { encoding: 'utf-8' });
    this.apply(JSON.parse(config));
    return this;
  }

  async save() {
    const payload = this.serialize();
    await writeFile(this.getConfigFile(), JSON.stringify(payload));
    return this;
  }

  serialize(): SerializedProject {
    return {
      created: this.created.toJSON(),
      description: this.description,
      sshHost: this.sshHost,
      sshKey: this.sshKey,
      sshPort: this.sshPort,
      sshTaskFile: this.sshTaskFile,
      sshUser: this.sshUser,
      title: this.title,
    };
  }

  static async tryLoad(name: string, path: string) {
    const project = new Project(name, path);

    try {
      return await project.load();
    } catch (error) {
      return null;
    }
  }
}
