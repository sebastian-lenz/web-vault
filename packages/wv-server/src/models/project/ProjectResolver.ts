import { Arg, Query, Resolver, Mutation } from 'type-graphql';

import Project from './Project';
import ProjectCollection from './ProjectCollection';
import ProjectPayload from './ProjectPayload';

@Resolver(of => Project)
export default class ProjectResolver {
  collection: ProjectCollection;

  constructor(collection: ProjectCollection) {
    this.collection = collection;
  }

  @Mutation(returns => Project)
  async createProject(@Arg('payload') payload: ProjectPayload) {
    const project = await this.collection.createProject(payload);
    return project.save();
  }

  @Query(returns => Project)
  project(@Arg('name') name: string) {
    return this.collection.findProject(name);
  }

  @Query(returns => [Project])
  async projects() {
    const { collection } = this;
    await collection.sync();
    return collection.getAllProjects();
  }

  @Mutation(returns => Project)
  async updateProject(
    @Arg('name') name: string,
    @Arg('payload') payload: ProjectPayload
  ) {
    const project = this.collection.findProject(name);
    project.apply(payload);
    return project.save();
  }

  @Mutation(returns => Boolean)
  async removeProject(@Arg('name') name: string) {
    return this.collection.removeProject(name);
  }
}
