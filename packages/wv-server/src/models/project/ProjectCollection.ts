import { join } from 'path';
import { readdir, stat } from 'fs-extra';
import Container, { Service } from 'typedi';

import Backup from '../../services/Backup';
import GlobalSettings from '../../services/GlobalSettings';
import Project from './Project';
import { remove, mkdir, pathExists } from 'fs-extra';
import Job from '../../services/Backup/Job';
import SSHKeyCollection from '../ssh-key/SSHKeyCollection';
import ProjectPayload from './ProjectPayload';

@Service()
export default class ProjectCollection {
  path: string;
  projects: Array<Project> = [];

  constructor({ dataPath }: GlobalSettings) {
    this.path = dataPath;
    this.sync();
  }

  async createProject(payload: ProjectPayload) {
    const { name, ...data } = payload;
    const path = this.createProjectPath(name);
    if ((await pathExists(path)) && (await readdir(path)).length) {
      throw new Error(
        `The project path "${name}" already exists and contains files.`
      );
    }

    const project = new Project(name, path);
    project.created = new Date();
    project.apply(data);

    // Just try whether this works
    try {
      const sshKeys = Container.get(SSHKeyCollection);
      await Job.loadConfig(sshKeys, project);
    } catch (error) {
      throw new Error(`Could not connect to target: ${error}`);
    }

    await mkdir(path);
    await project.save();

    this.projects.push(project);
    return project;
  }

  createProjectPath(name: string) {
    if (!this.isValidName(name)) {
      throw new Error(`Illegal project name "${name}".`);
    }

    return join(this.path, name);
  }

  getAllProjects(): Array<Project> {
    return this.projects;
  }

  isValidName(name: string): boolean {
    return /^[\.a-z0-9_-]+$/.test(name);
  }

  findProject(name: string): Project {
    const project = this.projects.find(project => project.name === name);
    if (!project) {
      throw new Error(`Could not find project "${name}".`);
    }

    return project;
  }

  async removeProject(name: string): Promise<Boolean> {
    const project = this.findProject(name);
    await remove(project.path);

    this.projects = this.projects.filter(p => p !== project);
    return true;
  }

  async sync() {
    const { path: basePath, projects } = this;
    const names = await readdir(basePath);
    const result: Array<Project> = [];

    for (const name of names) {
      const path = join(basePath, name);
      const pathStats = await stat(path);
      if (!pathStats.isDirectory()) {
        continue;
      }

      const index = projects.findIndex(project => project.path === path);
      if (index >= 0) {
        result.push(...projects.splice(index, 1));
      } else {
        const project = await Project.tryLoad(name, path);
        if (project) {
          result.push(project);
        }
      }
    }

    this.projects = result;
  }
}
