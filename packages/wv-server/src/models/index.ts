import ProjectResolver from './project/ProjectResolver';
import SSHKeyResolver from './ssh-key/SSHKeyResolver';

export const resolvers: any = [ProjectResolver, SSHKeyResolver];
