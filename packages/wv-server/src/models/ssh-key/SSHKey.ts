import { Arg, Field, ID, ObjectType, Query, Resolver } from 'type-graphql';

@ObjectType()
export default class SSHKey {
  @Field()
  name!: string;

  constructor(options: Partial<SSHKey> = {}) {
    Object.assign(this, options);
  }
}
