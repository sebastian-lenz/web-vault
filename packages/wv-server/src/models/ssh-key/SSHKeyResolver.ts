import { Query, Resolver } from 'type-graphql';

import SSHKey from './SSHKey';
import SSHKeyCollection from './SSHKeyCollection';

@Resolver(of => SSHKey)
export default class SSHKeyResolver {
  collection: SSHKeyCollection;

  constructor(collection: SSHKeyCollection) {
    this.collection = collection;
  }

  @Query(returns => [SSHKey])
  async sshKeys(): Promise<Array<SSHKey>> {
    return this.collection.getAllSSHKeys();
  }
}
