import { join } from 'path';
import { readdir, existsSync, mkdirpSync, chmodSync } from 'fs-extra';

import GlobalSettings from '../../services/GlobalSettings';
import spawn from '../../utils/spawn';
import SSHKey from './SSHKey';
import { Service } from 'typedi';

@Service()
export default class SSHKeyCollection {
  securePath: string;

  constructor({ securePath }: GlobalSettings) {
    this.securePath = securePath;
  }

  async ensureSSHKeys() {
    const keys = await this.getAllSSHKeys();
    if (keys.length) {
      return;
    }

    console.log('No ssh key found, generating a ssh key...');

    try {
      await spawn('ssh-keygen', [
        '-q',
        '-N',
        '',
        '-t',
        'rsa',
        '-b',
        '4096',
        '-f',
        join(this.securePath, 'id_rsa'),
      ]);
      console.log('Key generated');
    } catch (error) {
      console.error(`Could not generate key: ${error}`);
    }
  }

  async getAllSSHKeys(): Promise<Array<SSHKey>> {
    let files: Array<string>;
    try {
      files = await readdir(this.securePath);
    } catch (error) {
      console.error(error);
      files = [];
    }

    return files
      .filter(name => {
        return files.indexOf(`${name}.pub`) !== -1;
      })
      .map(name => {
        return new SSHKey({ name });
      });
  }

  getBasePath() {
    return this.securePath;
  }

  getPath(name: string) {
    return join(this.securePath, name);
  }
}
