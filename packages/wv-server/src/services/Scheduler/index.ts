import * as schedule from 'node-schedule';
import { Service } from 'typedi';

import Backup from '../Backup';

@Service()
export default class Scheduler {
  backup: Backup;
  job: schedule.Job;

  constructor(backup: Backup) {
    const rule = new schedule.RecurrenceRule();
    rule.hour = Math.floor(Math.random() * 3);
    rule.minute = Math.floor(Math.random() * 59);

    this.backup = backup;
    this.job = schedule.scheduleJob(rule, this.runBackup);

    console.log(
      `Scheduler started, next execution at ${this.job.nextInvocation()}`
    );
  }

  runBackup = async () => {
    await this.backup.executeAll();
  };
}
