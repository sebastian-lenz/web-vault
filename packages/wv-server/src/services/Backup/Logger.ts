import { writeJson } from 'fs-extra';

export type MessageType = 'info' | 'error' | 'success';

export interface PartialMessage {
  body: string;
  type: MessageType;
}

export interface Message extends PartialMessage {
  date: Date;
}

export interface MessageGroup {
  body: string;
  messages: Array<Message | MessageGroup>;
}

function isPartialMessage(value: any): value is PartialMessage {
  return (
    typeof value === 'object' &&
    typeof value.type === 'string' &&
    !('date' in value)
  );
}

export default class Logger {
  groups: Array<MessageGroup> = [];
  messages: Array<Message | MessageGroup> = [];
  parent?: Logger;

  constructor(parent?: Logger) {
    this.parent = parent;
  }

  beginGroup(body: string) {
    if (this.parent) {
      this.parent.beginGroup(body);
    }

    const group: MessageGroup = {
      body,
      messages: [],
    };

    this.log(group);
    this.groups.unshift(group);
  }

  endGroup() {
    if (this.parent) {
      this.parent.endGroup();
    }

    this.groups.shift();
  }

  error(body: string) {
    if (this.parent) {
      this.parent.error(body);
    }

    this.log({
      body,
      type: 'error',
    });
  }

  info(body: string) {
    if (this.parent) {
      this.parent.info(body);
    }

    this.log({
      body,
      type: 'info',
    });
  }

  log(message: Message | MessageGroup | PartialMessage) {
    const { messages, groups } = this;
    const target = groups.length ? groups[0].messages : messages;

    if (isPartialMessage(message)) {
      target.push({ ...message, date: new Date() });
    } else {
      target.push(message);
    }
  }

  success(body: string) {
    if (this.parent) {
      this.parent.success(body);
    }

    this.log({
      body,
      type: 'success',
    });
  }

  async save(path: string) {
    await writeJson(path, this.messages, { spaces: 2 });
  }
}
