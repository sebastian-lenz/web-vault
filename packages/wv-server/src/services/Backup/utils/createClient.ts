import { Client, ClientChannel, ExecOptions as SSHExecOptions } from 'ssh2';
import { readFile } from 'fs-extra';

import Project from '../../../models/project/Project';
import SSHKeyCollection from '../../../models/ssh-key/SSHKeyCollection';

function callback<T>(
  resolve: (value: T) => void,
  reject: (error: Error) => void
) {
  return (error: Error, result: T) => {
    if (error) {
      reject(error);
    } else {
      resolve(result);
    }
  };
}

function shellEscape(values: Array<string>): string {
  return values
    .map(value => {
      if (/[^A-Za-z0-9_\/:=-]/.test(value)) {
        value = "'" + value.replace(/'/g, "'\\''") + "'";
        value = value
          .replace(/^(?:'')+/g, '') // unduplicate single-quote at the beginning
          .replace(/\\'''/g, "\\'"); // remove non-escaped single-quote if there are enclosed between 2 escaped
      }

      return value;
    })
    .join(' ');
}

export interface Api {
  close(): void;
  exec(cmd: string, options?: ExecOptions): Promise<ExecResult>;
}

export interface ExecOptions {
  cwd?: string;
  options?: SSHExecOptions;
  parameters?: Array<string>;
  stdin?: string;
}

export interface ExecResult {
  stdout: string;
  stderr: string;
  code: number;
  signal?: string;
}

export default async function createClient(
  sshKeys: SSHKeyCollection,
  project: Project
): Promise<Api> {
  const path = sshKeys.getPath(project.sshKey);
  const privateKey = await readFile(path);
  let client: Client | null = new Client();

  function close() {
    if (client) {
      client.end();
      client = null;
    }
  }

  async function exec(
    command: string,
    { cwd, options = {}, parameters, stdin }: ExecOptions = {}
  ): Promise<ExecResult> {
    if (parameters) {
      command = [command].concat(shellEscape(parameters)).join(' ');
    }

    if (cwd) {
      command = `cd ${shellEscape([
        cwd,
      ])} 1> /dev/null 2> /dev/null; ${command}`;
    }

    return new Promise((resolve, reject) => {
      if (!client) {
        return reject('No connection');
      }

      const stdout: Array<Buffer> = [];
      const stderr: Array<Buffer> = [];
      const handler = (channel: ClientChannel) => {
        channel.on('data', (chunk: Buffer) => {
          stdout.push(chunk);
        });

        channel.stderr.on('data', (chunk: Buffer) => {
          stderr.push(chunk);
        });

        if (stdin) {
          channel.write(stdin);
          channel.end();
        }

        channel.on('close', (code: number, signal: string) => {
          resolve({
            code,
            signal,
            stdout: stdout.join('').trim(),
            stderr: stderr.join('').trim(),
          });
        });
      };

      client.exec(command, options, callback(handler, reject));
    });
  }

  return new Promise((resolve, reject) => {
    client = new Client();
    client.on('error', reject);
    client.once('ready', () => {
      if (client) {
        client.off('error', reject);
        resolve({ close, exec });
      } else {
        reject();
      }
    });

    client.connect({
      host: project.sshHost,
      port: project.sshPort,
      username: project.sshUser,
      privateKey,
    });
  });
}
