import { join } from 'path';
import {
  readdir,
  remove,
  stat,
  rename,
  mkdir,
  link,
  pathExists,
  Stats,
} from 'fs-extra';

import Logger from './Logger';
import spawn from '../../utils/spawn';

const pattern = /^(daily|weekly|monthly)\.(\d+)$/;

const intervals: Array<Interval> = ['daily', 'weekly', 'monthly'];

function getLowerInteval(interval: Interval): Interval | null {
  switch (interval) {
    case 'monthly':
      return 'weekly';
    case 'weekly':
      return 'daily';
    default:
      return null;
  }
}

async function linkRecursive(source: string, target: string) {
  const names = await readdir(source);
  for (const name of names) {
    const sourcePath = join(source, name);
    const targetPath = join(target, name);
    let sourceStat: Stats | undefined;
    let targetStat: Stats | undefined;

    await Promise.all([
      stat(sourcePath)
        .then(stat => (sourceStat = stat))
        .catch(() => null),
      stat(targetPath)
        .then(stat => (targetStat = stat))
        .catch(() => null),
    ]);

    if (!sourceStat || targetStat) {
      continue;
    }

    if (sourceStat.isDirectory()) {
      await mkdir(targetPath);
      await linkRecursive(sourcePath, targetPath);
    } else {
      await link(sourcePath, targetPath);
    }
  }
}

export type Interval = 'daily' | 'weekly' | 'monthly';

export type PathInfo = {
  name: string;
  offset: number;
  path: string;
};

export type PathMap = {
  [interval in Interval]: Array<PathInfo>;
};

export type Settings = {
  [interval in Interval]: IntervalSettings;
};

export type IntervalSettings = {
  numBackups: number;
};

export default class History {
  basePath: string;
  paths: PathMap;
  settings: Settings = {
    daily: { numBackups: 7 },
    weekly: { numBackups: 4 },
    monthly: { numBackups: 12 },
  };

  constructor(basePath: string, paths: PathMap) {
    this.basePath = basePath;
    this.paths = paths;
  }

  private async createInterval(
    logger: Logger,
    interval: Interval
  ): Promise<PathInfo> {
    const { basePath } = this;
    const name = `${interval}.0`;
    const path = join(basePath, `${interval}.0`);

    logger.info(`Create backup "${name}"`);
    await mkdir(path);

    const source = this.getLowerIntevalPath(interval);
    if (source) {
      logger.info(`Linking "${source.path}" to "${path}"`);
      await linkRecursive(source.path, path);
    }

    return { name, offset: 0, path };
  }

  getLatest(): PathInfo | null {
    const { daily } = this.paths;
    return daily.length ? daily[0] : null;
  }

  async getLatestDate(): Promise<null | Date> {
    const latest = this.getLatest();
    if (!latest) {
      return null;
    }

    const latestStat = await stat(latest.path);
    return latestStat.ctime;
  }

  getLatestPath(): string | null {
    const latest = this.getLatest();
    return latest ? latest.path : null;
  }

  getLowerIntevalPath(interval: Interval): PathInfo | null {
    const lower = getLowerInteval(interval);
    const paths = lower ? this.paths[lower] : null;
    return paths && paths.length ? paths[0] : null;
  }

  async getSizeOnDisk(): Promise<number> {
    const result = await spawn('du', ['-s', this.basePath]);
    const match = /^(\d+)/.exec(result);
    if (!match) {
      throw new Error();
    }

    return parseInt(result[1]);
  }

  async isUpToDate() {
    const latest = await this.getLatestDate();
    const now = new Date();

    if (latest !== null) {
      if (
        now.getFullYear() === latest.getFullYear() &&
        now.getMonth() === latest.getMonth() &&
        now.getDate() === latest.getDate()
      ) {
        return true;
      }
    }

    return false;
  }

  async linkFromPrevious(name: string): Promise<string> {
    const { daily } = this.paths;
    if (!daily.length) {
      throw new Error(
        'No backup target exists, History::rotate must be called first.'
      );
    }

    const target = join(daily[0].path, name);
    mkdir(target);

    if (daily.length > 1) {
      const source = join(daily[1].path, name);
      if (await pathExists(source)) {
        await linkRecursive(source, target);
      }
    }

    return target;
  }

  private async removePath(logger: Logger, info: PathInfo) {
    logger.info(`Removing backup "${info.name}"`);
    await remove(info.path);
  }

  async rotate(logger: Logger) {
    if (await this.isUpToDate()) {
      throw new Error('Backups have already been rotated today.');
    }

    const now = new Date();
    if (now.getDate() === 1) {
      logger.beginGroup('Monthly backup rotation');
      await this.rotateInterval(logger, 'monthly');
      logger.endGroup();
    }

    if (now.getDay() === 1) {
      logger.beginGroup('Weekly backup rotation');
      await this.rotateInterval(logger, 'weekly');
      logger.endGroup();
    }

    logger.beginGroup('Daily backup rotation');
    await this.rotateInterval(logger, 'daily');
    logger.endGroup();
  }

  private async rotateInterval(logger: Logger, interval: Interval) {
    const { basePath } = this;
    const { numBackups } = this.settings[interval];
    const pathInfos = this.paths[interval];

    while (numBackups > 0 && pathInfos.length > numBackups) {
      const pathInfo = pathInfos.pop();
      if (pathInfo) {
        await this.removePath(logger, pathInfo);
      }
    }

    for (const { path } of pathInfos) {
      const tmpName = `${path}.tmp`;
      await rename(path, tmpName);
    }

    for (const pathInfo of pathInfos) {
      const oldName = pathInfo.name;
      const tmpName = `${pathInfo.path}.tmp`;

      pathInfo.offset += 1;
      pathInfo.name = `${interval}.${pathInfo.offset}`;
      pathInfo.path = join(basePath, pathInfo.name);

      logger.info(`Renaming backup "${oldName}" to "${pathInfo.name}"`);
      await rename(tmpName, pathInfo.path);
    }

    pathInfos.unshift(await this.createInterval(logger, interval));
  }

  static async create(basePath: string) {
    const names = await readdir(basePath);
    const paths: PathMap = {
      daily: [],
      monthly: [],
      weekly: [],
    };

    for (const name of names) {
      const match = pattern.exec(name);
      if (!match) {
        continue;
      }

      const path = join(basePath, name);
      const pathStat = await stat(path);
      if (!pathStat.isDirectory()) {
        throw new Error(`Backup path ${path} exists but is not a directory.`);
      }

      paths[match[1] as Interval].push({
        name,
        offset: parseInt(match[2]),
        path,
      });
    }

    for (const interval of intervals) {
      paths[interval].sort((left, right) => left.offset - right.offset);
    }

    return new History(basePath, paths);
  }
}
