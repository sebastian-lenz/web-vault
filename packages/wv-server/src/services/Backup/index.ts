import { join } from 'path';
import { Service } from 'typedi';

import Job from './Job';
import Logger from './Logger';
import Project from '../../models/project/Project';
import ProjectCollection from '../../models/project/ProjectCollection';
import SSHKeyCollection from '../../models/ssh-key/SSHKeyCollection';
import isOnline from '../../utils/isOnline';

@Service()
export default class Backup {
  projects: ProjectCollection;
  sshKeys: SSHKeyCollection;

  constructor(projects: ProjectCollection, sshKeys: SSHKeyCollection) {
    this.projects = projects;
    this.sshKeys = sshKeys;
  }

  async createJob(project: Project, logger?: Logger): Promise<Job> {
    return Job.create(this.sshKeys, project, logger);
  }

  async executeAll() {
    const { projects } = this;
    const logger = new Logger();

    await projects.sync();

    try {
      await isOnline();
    } catch (error) {
      logger.error('No internet connection!');
    }

    for (const project of projects.getAllProjects()) {
      try {
        logger.beginGroup(`Running project "${project.name}"`);
        const job = await this.createJob(project, logger);

        if (await job.history.isUpToDate()) {
          logger.info('Project is already up to date');
        } else {
          await job.execute();
        }
      } catch (error) {
        logger.error(`${error}`);
      } finally {
        logger.endGroup();
      }
    }

    const path = join(this.projects.path, '.wvlog');
    logger.save(path);
  }
}
