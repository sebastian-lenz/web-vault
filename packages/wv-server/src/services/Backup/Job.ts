import { join } from 'path';
import { spawn, ChildProcessWithoutNullStreams } from 'child_process';

import createClient from './utils/createClient';
import createTasks, { Task, TaskConfigMap } from './tasks';
import History from './History';
import Logger from './Logger';
import Project from '../../models/project/Project';
import SSHKeyCollection from '../../models/ssh-key/SSHKeyCollection';

export interface JobConfig {
  scripts?: {
    prebackup?: string;
  };
  tasks: TaskConfigMap;
}

export default class Job {
  config: JobConfig;
  project: Project;
  history: History;
  logger: Logger;
  sshKeys: SSHKeyCollection;
  tasks: Array<Task> = [];

  constructor(
    sshKeys: SSHKeyCollection,
    project: Project,
    history: History,
    config: JobConfig,
    logger?: Logger
  ) {
    this.config = config;
    this.project = project;
    this.history = history;
    this.logger = new Logger(logger);
    this.sshKeys = sshKeys;
    this.tasks = createTasks(this, config.tasks);
  }

  async init() {
    const { config, logger } = this;
    const { scripts = {} } = config;
    const { prebackup } = scripts;

    if (prebackup) {
      logger?.beginGroup('Executing pre backup script');
      try {
        await this.ssh(prebackup);
      } finally {
        logger?.endGroup();
      }
    }
  }

  async execute() {
    const { history, logger, tasks } = this;

    await history.rotate(logger);
    const pathInfo = history.getLatest();
    if (!pathInfo) {
      throw new Error(`Could not create backup target.`);
    }

    for (const task of tasks) {
      logger.beginGroup(`Running task "${task.name}"`);

      try {
        await task.execute();
      } catch (error) {
        logger.error(error.message);
      }

      logger.endGroup();
    }

    await logger.save(join(pathInfo.path, '.vwlog'));
  }

  async ssh(
    cmd: string,
    callback?: (process: ChildProcessWithoutNullStreams) => void
  ) {
    const { project, sshKeys } = this;
    const { sshHost, sshKey, sshPort, sshUser } = project;

    return new Promise<void>((resolve, reject) => {
      const stdError: Array<string> = [];

      const process = spawn('ssh', [
        '-t',
        '-i',
        sshKeys.getPath(sshKey),
        '-o',
        'StrictHostKeyChecking=no',
        '-p',
        `${sshPort}`,
        `${sshUser}@${sshHost}`,
        cmd,
      ]);

      process.stderr.on('data', chunk => stdError.push(`${chunk}`));

      process
        .on('exit', (code: number, signal: string) => {
          if (code) {
            reject(code + ': ' + stdError.join(''));
          } else {
            resolve();
          }
        })
        .on('error', error => {
          reject(error);
        });

      if (callback) {
        callback(process);
      }
    });
  }

  static async create(
    sshKeys: SSHKeyCollection,
    project: Project,
    logger?: Logger
  ): Promise<Job> {
    const config = await this.loadConfig(sshKeys, project, logger);
    const history = await History.create(project.path);

    const job = new Job(sshKeys, project, history, config, logger);
    await job.init();
    return job;
  }

  static async loadConfig(
    sshKeys: SSHKeyCollection,
    project: Project,
    logger?: Logger
  ): Promise<JobConfig> {
    const client = await createClient(sshKeys, project);
    const taskFile = project.getTaskFile();

    try {
      const configResult = await client.exec('cat', {
        parameters: [taskFile],
      });

      if (configResult.code !== 0) {
        throw new Error(configResult.stderr || 'Unknown error');
      }

      return JSON.parse(configResult.stdout) as JobConfig;
    } catch (error) {
      throw new Error(`Could not read task file: "${taskFile}": ${error}`);
    } finally {
      client.close();
    }
  }
}
