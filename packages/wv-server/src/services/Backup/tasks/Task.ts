import Job from '../Job';

export interface TaskConfig {
  type: string;
}

export default class Task<TConfig extends TaskConfig = TaskConfig> {
  config: TConfig;
  job: Job;
  name: string;
  priority: number = 0;

  constructor(job: Job, name: string, config: TConfig) {
    this.job = job;
    this.name = name;
    this.config = config;
  }

  async execute() {}
}
