import { createWriteStream } from 'fs';
import { join } from 'path';
import { mkdir } from 'fs-extra';
import { quote } from 'shell-quote';

import Task, { TaskConfig } from './Task';

export interface MysqlDumpConfig extends TaskConfig {
  type: 'mysqldump';
  host?: string;
  user?: string;
  password?: string;
  database?: string;
}

export default class MysqlDump extends Task<MysqlDumpConfig> {
  async execute() {
    const { job } = this;
    const { history, logger } = job;

    const basePath = history.getLatestPath();
    if (!basePath) {
      throw new Error('Could not find backup target.');
    }

    const command = this.getRemoteCommand();
    logger.info(`Executing command: ${command}`);

    const targetPath = join(basePath, this.name);
    await mkdir(targetPath);

    const target = join(targetPath, 'dump.sql');
    const stream = createWriteStream(target);

    return job.ssh(command, process => {
      process.stdout.pipe(stream);
    });
  }

  private getRemoteCommand() {
    const { database, host, password, user } = this.config;
    const args: Array<string> = [];

    if (user) {
      args.push('-u', user);
    }

    if (password) {
      args.push(`-p${password}`);
    }

    if (host) {
      args.push('-h', host);
    }

    if (database) {
      args.push(database);
    }

    return `mysqldump ${quote(args)}`;
  }
}
