import MysqlDumpTask, { MysqlDumpConfig } from './MysqlDumpTask';
import RsyncTask, { RsyncConfig } from './RsyncTask';
import Task, { TaskConfig } from './Task';
import Job from '../Job';

export { Task, TaskConfig };

export type AnyTaskConfig = MysqlDumpConfig | RsyncConfig;

export interface TaskConfigMap {
  [name: string]: AnyTaskConfig;
}

export function createTask(
  job: Job,
  name: string,
  config: AnyTaskConfig
): Task | null {
  switch (config.type) {
    case 'mysqldump':
      return new MysqlDumpTask(job, name, config);
    case 'rsync':
      return new RsyncTask(job, name, config);
  }

  return null;
}

export default function createTasks(
  job: Job,
  configs: TaskConfigMap
): Array<Task> {
  const tasks: Array<Task> = [];

  for (const name in configs) {
    const task = createTask(job, name, configs[name]);
    if (task) {
      tasks.push(task);
    }
  }

  return tasks;
}
