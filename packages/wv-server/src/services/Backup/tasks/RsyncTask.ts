import * as Rsync from 'rsync';

import Task, { TaskConfig } from './Task';

export interface RsyncConfig extends TaskConfig {
  type: 'rsync';
  path: string;
}

export default class RsyncTask extends Task<RsyncConfig> {
  async execute() {
    const { config, job, name } = this;
    const { logger } = job;
    const { path } = config;
    const { sshHost, sshKey, sshPort, sshUser } = job.project;
    const sshKeyPath = job.sshKeys.getPath(sshKey);
    const destination = await job.history.linkFromPrevious(name);
    const stdOut: Array<string> = [];
    const stdError: Array<string> = [];
    const rsync = new Rsync();

    rsync
      .flags('a')
      .set(
        'e',
        `ssh -i ${sshKeyPath} -o StrictHostKeyChecking=no -p ${sshPort}`
      )
      .source(`${sshUser}@${sshHost}:${path}`)
      .destination(destination);

    logger.info(`Executing command: ${rsync.command()}`);

    return new Promise<void>((resolve, reject) => {
      rsync.execute(
        error => {
          if (stdOut.length) logger.info(stdOut.join(''));
          if (stdError.length) logger.error(stdError.join(''));

          if (error) {
            reject(error);
          } else {
            resolve();
          }
        },
        value => stdOut.push(`${value}`),
        value => stdError.push(`${value}`)
      );
    });
  }
}
