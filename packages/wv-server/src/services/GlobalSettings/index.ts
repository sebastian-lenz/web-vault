import { Service } from 'typedi';
import { Options } from '../../utils/readOptions';

@Service()
export default class GlobalSettings {
  dataPath: string = '';
  securePath: string = '';

  apply(options: Options) {
    Object.assign(this, options);
  }
}
