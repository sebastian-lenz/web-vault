import * as React from 'react';
import { GraphQLError } from 'graphql';
import { makeStyles } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core';

import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import SaveIcon from '@material-ui/icons/Save';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import slugify from '../utils/slugify';
import useSSHKeys from '../gql/useSshKeys';
import { ProjectFragment, ProjectPayload } from '../gql/types/Project';

const useStyles = makeStyles((theme: Theme) => ({
  projectForm: {
    position: 'relative',
    padding: theme.spacing(3),
  },
  projectInput: {
    marginBottom: theme.spacing(3),
  },
  projectActions: {
    marginTop: theme.spacing(2),
    textAlign: 'right',
  },
  projectAction: {
    marginLeft: theme.spacing(1),
  },
  projectProgress: {
    position: 'absolute',
    top: 'calc(50% - 20px)',
    left: 'calc(50% - 20px)',
  },
}));

export interface Props {
  errors: ReadonlyArray<GraphQLError>;
  isLoading?: boolean;
  onCancel: () => void;
  onSave: (project: ProjectPayload) => Promise<void>;
  project: ProjectFragment;
}

export default function({
  errors,
  isLoading,
  onCancel,
  onSave,
  project,
}: Props) {
  const styles = useStyles();
  const { data: sshData } = useSSHKeys();

  const [hasAutoName, setHasAutoName] = React.useState(
    () => project.name === ''
  );

  const [value, setValue] = React.useState<ProjectFragment>(() => ({
    ...project,
  }));

  return (
    <Paper className={styles.projectForm}>
      <Typography variant="h6">Project settings</Typography>

      {errors.map((error, index) => (
        <Typography color="error" gutterBottom key={index}>
          {error.message}
        </Typography>
      ))}

      <TextField
        className={styles.projectInput}
        disabled={isLoading}
        fullWidth
        helperText="The name of this project within the overview."
        label="Title"
        onChange={event =>
          setValue({
            ...value,
            name: hasAutoName ? slugify(event.target.value) : value.name,
            title: event.target.value,
          })
        }
        required
        value={value.title}
      />
      <TextField
        className={styles.projectInput}
        disabled={isLoading}
        fullWidth
        helperText="Defines the name of the directory the backup will be stored to."
        label="Name"
        onChange={event => {
          setHasAutoName(false);
          setValue({
            ...value,
            name: event.target.value.replace(/[^a-z0-9\._-]+/g, ''),
          });
        }}
        required
        value={value.name}
      />
      <TextField
        className={styles.projectInput}
        disabled={isLoading}
        fullWidth
        helperText="Additional description and notes"
        multiline
        label="Description"
        onChange={event =>
          setValue({ ...value, description: event.target.value })
        }
        value={value.description}
      />

      <Box marginTop={3}>
        <Typography variant="h6">Server settings</Typography>
      </Box>

      <TextField
        className={styles.projectInput}
        disabled={isLoading}
        fullWidth
        helperText="The remote host address, e.g. server-name.com."
        label="Remote address"
        onChange={event => setValue({ ...value, sshHost: event.target.value })}
        value={value.sshHost}
      />
      <TextField
        className={styles.projectInput}
        disabled={isLoading}
        fullWidth
        helperText="The port the connection should use, defaults to 22."
        label="Remote port"
        onChange={event =>
          setValue({ ...value, sshPort: parseInt(event.target.value) })
        }
        value={value.sshPort}
      />
      <TextField
        className={styles.projectInput}
        disabled={isLoading}
        fullWidth
        helperText="The name of the user that will be used to open the connection."
        label="User name"
        onChange={event => setValue({ ...value, sshUser: event.target.value })}
        value={value.sshUser}
      />
      <TextField
        className={styles.projectInput}
        disabled={isLoading}
        fullWidth
        helperText="The encryption key that will be used for authentication."
        label="Key file"
        onChange={event => setValue({ ...value, sshKey: event.target.value })}
        select
        value={value.sshKey}
      >
        {sshData
          ? sshData.sshKeys.map(key => (
              <MenuItem key={key.name} value={key.name}>
                {key.name}
              </MenuItem>
            ))
          : null}
      </TextField>
      <TextField
        className={styles.projectInput}
        disabled={isLoading}
        fullWidth
        helperText="The path to the task configutation. Defaults to ~/.wvtasks"
        label="Task file"
        onChange={event =>
          setValue({ ...value, sshTaskFile: event.target.value })
        }
        value={value.sshTaskFile}
      />

      <div className={styles.projectActions}>
        <Button
          className={styles.projectAction}
          disabled={isLoading}
          onClick={onCancel}
        >
          Cancel
        </Button>
        <Button
          className={styles.projectAction}
          color="primary"
          disabled={isLoading}
          onClick={() =>
            onSave({
              description: value.description,
              name: value.name,
              sshHost: value.sshHost,
              sshKey: value.sshKey,
              sshPort: value.sshPort,
              sshTaskFile: value.sshTaskFile,
              sshUser: value.sshUser,
              title: value.title,
            })
          }
          startIcon={<SaveIcon />}
          variant="contained"
        >
          Save
        </Button>
      </div>

      {isLoading ? (
        <CircularProgress className={styles.projectProgress} />
      ) : null}
    </Paper>
  );
}
