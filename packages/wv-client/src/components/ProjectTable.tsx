import * as React from 'react';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Link } from 'react-router-dom';

import useProjects from '../gql/useProjects';
import { ProjectFragment } from '../gql/types/Project';

export interface Props {
  onDelete: (project: ProjectFragment) => void;
}
export default function ProjectTable({ onDelete }: Props) {
  const { loading, error, data } = useProjects();
  if (loading) return <div>Error!</div>;
  if (error) return <div>Error!</div>;
  if (!data) return <div>Error!</div>;

  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Title</TableCell>
          <TableCell align="right">Actions</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {data.projects.map(project => (
          <TableRow key={project.name}>
            <TableCell component="th" scope="row">
              <Link to={`/projects/edit/${project.name}`}>{project.title}</Link>
            </TableCell>
            <TableCell align="right">
              <Button onClick={() => onDelete(project)}>
                <DeleteIcon />
              </Button>
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
}
