const replacements = [
  { search: /\s+/g, replace: '-' },
  { search: /ä/g, replace: 'ae' },
  { search: /ö/g, replace: 'oe' },
  { search: /ü/g, replace: 'ue' },
  { search: /ß/g, replace: 'ss' },
  { search: /--+/g, replace: '-' },
];

export default function slugify(value: string): string {
  let result = value.toLocaleLowerCase();
  for (const { search, replace } of replacements) {
    result = result.replace(search, replace);
  }

  return result
    .replace(/[^a-z0-9\._-]+/g, '')
    .replace(/^[_-]+/, '')
    .replace(/[_-]+$/, '');
}
