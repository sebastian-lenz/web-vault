import gql from 'graphql-tag';
import { useMutation, MutationHookOptions } from '@apollo/react-hooks';

import { getProjectsQuery } from './useProjects';

import {
  projectFragment,
  ProjectFragment,
  ProjectPayload,
} from './types/Project';

export interface CreateProjectData {
  createProject: ProjectFragment;
}

export interface CreateProjectVariables {
  payload: ProjectPayload;
}

export const createProjectMutation = gql`
  ${projectFragment}
  mutation createProject($payload: ProjectPayload!) {
    createProject(payload: $payload) {
      ...ProjectFragment
    }
  }
`;

export default function useCreateProject(
  options: MutationHookOptions<CreateProjectData, CreateProjectVariables> = {}
) {
  return useMutation<CreateProjectData, CreateProjectVariables>(
    createProjectMutation,
    {
      ...options,
      refetchQueries: () => [{ query: getProjectsQuery }],
    }
  );
}
