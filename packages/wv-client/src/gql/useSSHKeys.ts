import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';

import { SSHKeyFragment, sshKeyFragment } from './types/SSHKey';

export interface GetSSHKeysData {
  sshKeys: SSHKeyFragment[];
}

export const getSSHKeysQuery = gql`
  ${sshKeyFragment}
  query getSSHKeys {
    sshKeys {
      ...SSHKeyFragment
    }
  }
`;

export default function useSSHKeys() {
  return useQuery<GetSSHKeysData>(getSSHKeysQuery);
}
