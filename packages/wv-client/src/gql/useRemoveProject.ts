import gql from 'graphql-tag';
import { useMutation, MutationHookOptions } from '@apollo/react-hooks';

import { getProjectsQuery } from './useProjects';

export interface RemoveProjectData {
  removeProject: boolean;
}

export interface RemoveProjectVariables {
  name: string;
}

export const removeProjectMutation = gql`
  mutation removeProject($name: String!) {
    removeProject(name: $name)
  }
`;

export default function useRemoveProject(
  options: MutationHookOptions<RemoveProjectData, RemoveProjectVariables> = {}
) {
  return useMutation<RemoveProjectData, RemoveProjectVariables>(
    removeProjectMutation,
    {
      ...options,
      refetchQueries: () => [{ query: getProjectsQuery }],
    }
  );
}
