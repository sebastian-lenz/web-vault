import gql from 'graphql-tag';

export const sshKeyFragment = gql`
  fragment SSHKeyFragment on SSHKey {
    name
  }
`;

export interface SSHKeyFragment {
  name: string;
}
