import gql from 'graphql-tag';

export const projectFragment = gql`
  fragment ProjectFragment on Project {
    created
    description
    name
    title
    sshHost
    sshKey
    sshPort
    sshTaskFile
    sshUser
  }
`;

export interface ProjectFragment {
  created: Date;
  description: string;
  name: string;
  title: string;
  sshHost: string;
  sshKey: string;
  sshPort: number;
  sshTaskFile: string;
  sshUser: string;
}

export interface ProjectPayload {
  description: string;
  name: string;
  title: string;
  sshHost: string;
  sshKey: string;
  sshPort: number;
  sshTaskFile: string;
  sshUser: string;
}
