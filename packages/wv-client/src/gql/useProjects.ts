import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';

import { ProjectFragment, projectFragment } from './types/Project';

export interface GetProjectsData {
  projects: ProjectFragment[];
}

export const getProjectsQuery = gql`
  ${projectFragment}
  query getProjects {
    projects {
      ...ProjectFragment
    }
  }
`;

export default function useProjects() {
  return useQuery<GetProjectsData>(getProjectsQuery);
}
