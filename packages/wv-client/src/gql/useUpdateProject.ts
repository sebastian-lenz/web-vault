import gql from 'graphql-tag';
import { useMutation, MutationHookOptions } from '@apollo/react-hooks';

import { getProjectQuery } from './useProject';
import { getProjectsQuery } from './useProjects';
import {
  projectFragment,
  ProjectFragment,
  ProjectPayload,
} from './types/Project';

export interface UpdateProjectData {
  updateProject: ProjectFragment;
}

export interface UpdateProjectVariables {
  name: string;
  payload: ProjectPayload;
}

export const updateProjectMutation = gql`
  ${projectFragment}
  mutation updateProject($name: String!, $payload: ProjectPayload!) {
    updateProject(name: $name, payload: $payload) {
      ...ProjectFragment
    }
  }
`;

export default function useUpdateProject(
  options: MutationHookOptions<UpdateProjectData, UpdateProjectVariables> = {}
) {
  return useMutation<UpdateProjectData, UpdateProjectVariables>(
    updateProjectMutation,
    {
      ...options,
      refetchQueries: () => [{ query: getProjectsQuery }],
    }
  );
}
