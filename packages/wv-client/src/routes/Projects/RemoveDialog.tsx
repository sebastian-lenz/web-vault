import * as React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import { ProjectFragment } from '../../gql/types/Project';

export interface Props {
  onCancel: () => void;
  onRemove: () => void;
  project: ProjectFragment | null;
}

export default function RemoveDialog({ onCancel, onRemove, project }: Props) {
  const [lastProject, setLastProject] = React.useState<ProjectFragment | null>(
    project
  );

  if (project !== null && lastProject !== project) {
    setLastProject(project);
  }

  return (
    <Dialog open={!!project} onClose={onCancel}>
      <DialogTitle>
        {project ? `Remove project "${project.title}"?` : 'Remove project?'}
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          Are you sure you want to delete the selected project? All related
          files and backups will be deleted.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancel}>Cancel</Button>
        <Button onClick={onRemove} color="primary" autoFocus>
          Remove project
        </Button>
      </DialogActions>
    </Dialog>
  );
}
