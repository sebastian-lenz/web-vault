import * as React from 'react';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { useHistory } from 'react-router';

import ProjectTable from '../../components/ProjectTable';
import RemoveDialog from './RemoveDialog';
import useRemoveProject from '../../gql/useRemoveProject';
import { ProjectFragment } from '../../gql/types/Project';

export default function Projects() {
  const history = useHistory();
  const [removeProject] = useRemoveProject();
  const [
    projectToRemove,
    setProjectToRemove,
  ] = React.useState<ProjectFragment | null>(null);

  return (
    <Paper>
      <ProjectTable onDelete={project => setProjectToRemove(project)} />
      <Box padding={2} textAlign="right">
        <Button
          color="primary"
          onClick={() => history.push('/projects/create')}
          variant="contained"
        >
          Add project
        </Button>
      </Box>

      <RemoveDialog
        onCancel={() => setProjectToRemove(null)}
        onRemove={async function() {
          setProjectToRemove(null);
          if (projectToRemove === null) return;

          const result = await removeProject({
            variables: { name: projectToRemove.name },
          });
        }}
        project={projectToRemove}
      />
    </Paper>
  );
}
