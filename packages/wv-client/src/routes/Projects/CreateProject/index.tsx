import * as React from 'react';
import { ApolloError } from 'apollo-boost';
import { GraphQLError } from 'graphql';
import { useHistory } from 'react-router';

import ProjectForm from '../../../components/ProjectForm';
import useCreateProject from '../../../gql/useCreateProject';
import { ProjectPayload } from '../../../gql/types/Project';

export interface State {
  isLoading: boolean;
  errors: ReadonlyArray<GraphQLError>;
}

export default function CreateProject() {
  const history = useHistory();
  const [createProject] = useCreateProject();
  const [state, setState] = React.useState<State>({
    errors: [],
    isLoading: false,
  });

  return (
    <ProjectForm
      {...state}
      onCancel={() => {
        history.push('/');
      }}
      onSave={async function(payload: ProjectPayload) {
        try {
          setState({
            ...state,
            isLoading: true,
          });

          await createProject({ variables: { payload } });
          history.push('/');
        } catch (error) {
          if (error instanceof ApolloError) {
            setState({
              errors: error.graphQLErrors,
              isLoading: false,
            });
          }
        }
      }}
      project={{
        created: new Date(),
        description: '',
        name: '',
        sshHost: '',
        sshKey: '',
        sshPort: 22,
        sshTaskFile: '',
        sshUser: '',
        title: '',
      }}
    />
  );
}
