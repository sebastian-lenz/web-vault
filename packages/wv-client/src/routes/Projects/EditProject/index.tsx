import * as React from 'react';
import { RouteChildrenProps, useHistory } from 'react-router';

import ProjectForm from '../../../components/ProjectForm';
import useProjects from '../../../gql/useProjects';
import useUpdateProject from '../../../gql/useUpdateProject';
import { ProjectPayload } from '../../../gql/types/Project';
import { GraphQLError } from 'graphql';
import { ApolloError } from 'apollo-boost';

export interface Props extends RouteChildrenProps<{ name: string }> {}

export interface State {
  isLoading: boolean;
  errors: ReadonlyArray<GraphQLError>;
}

export default function EditProject({ match }: Props) {
  if (!match || !match.params.name) {
    return <div>No Name</div>;
  }

  const history = useHistory();
  const [updateProject] = useUpdateProject();
  const [state, setState] = React.useState<State>({
    errors: [],
    isLoading: false,
  });

  const { name } = match.params;
  const { data, loading } = useProjects();
  const project = data
    ? data.projects.find(project => project.name === name)
    : undefined;

  if (!project) {
    return <div>Error</div>;
  }

  return (
    <ProjectForm
      errors={state.errors}
      isLoading={state.isLoading || loading}
      onCancel={() => {
        history.push('/');
      }}
      onSave={async function(payload: ProjectPayload) {
        setState({ ...state, isLoading: true });
        try {
          await updateProject({
            variables: { name, payload },
          });
          history.push('/');
        } catch (error) {
          if (error instanceof ApolloError) {
            setState({ errors: error.graphQLErrors, isLoading: false });
          }
        }
      }}
      project={project}
    />
  );
}
