export default {
  '@font-face': [
    {
      fontFamily: 'Roboto',
      fontStyle: 'normal',
      fontWeight: 300,
      src: [
        "local('Roboto Light')",
        "local('Roboto-Light')",
        "url('/fonts/roboto-300.woff2') format('woff2')",
        "url('/fonts/roboto-300.woff') format('woff')",
      ].join(','),
    },
    {
      fontFamily: 'Roboto',
      fontStyle: 'normal',
      fontWeight: 400,
      src: [
        "local('Roboto')",
        "local('Roboto-Regular')",
        "url('/fonts/roboto-400.woff2') format('woff2')",
        "url('/fonts/roboto-400.woff') format('woff')",
      ].join(','),
    },
    {
      fontFamily: 'Roboto',
      fontStyle: 'normal',
      fontWeight: 500,
      src: [
        "local('Roboto Medium')",
        "local('Roboto-Medium')",
        "url('/fonts/roboto-500.woff2') format('woff2')",
        "url('/fonts/roboto-500.woff') format('woff')",
      ].join(','),
    },
  ],
  body: {
    fontFamily: 'Roboto',
  },
  '.material-icons': {
    fontFamily: 'Material Icons',
    fontWeight: 'normal',
    fontStyle: 'normal',
    fontSize: '24px',
    lineHeight: 1,
    letterSpacing: 'normal',
    textTransform: 'none',
    display: 'inline-block',
    whiteSpace: 'nowrap',
    wordWrap: 'normal',
    direction: 'ltr',
    '-webkit-font-feature-settings': 'liga',
    '-webkit-font-smoothing': 'antialiased',
  },
};
