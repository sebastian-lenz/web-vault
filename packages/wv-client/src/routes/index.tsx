import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Route, Switch } from 'react-router';

import globalStyles from './globalStyles';

import AppPanel from '../components/AppPanel';
import CreateProject from './Projects/CreateProject';
import Projects from './Projects';
import EditProject from './Projects/EditProject';
import useProjects from '../gql/useProjects';

const useStyles = makeStyles(
  {
    '@global': globalStyles,
  },
  { name: 'wvGlobals' }
);

export default function Routes() {
  useStyles();

  return (
    <AppPanel>
      <Switch>
        <Route component={CreateProject} path="/projects/create" />
        <Route component={EditProject} path="/projects/edit/:name" />
        <Route component={Projects} path="/" />
      </Switch>
    </AppPanel>
  );
}
